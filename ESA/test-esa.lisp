(in-package :esa)

(defun esa-partial-command-parser (command-table stream command position
                                   &optional numeric-argument)




(funcall *partial-command-parser*
                                        (command-table command-processor)
                                        *standard-input*
                                        command 0 (when prefix-p prefix-arg)))


define-command-table



CLIM Command Tables
CLIM command tables are represented by instances of the CLOS class
clim:command-table. A command table serves to mediate between a command in-
put context, a set of commands and the interactions of the application’s user.
Command tables associate command names with command line names. Command
line names are used in the command line interaction style. They are the textual
representation of the command name when presented and accepted.
A command table can describe a menu from which users can choose commands. A
command table can support keystroke accelerators for invoking commands.
A command table can have a set of presentation translators and actions, defined by
clim:define-presentation-translator,
 clim:define-presentation-to-command-
translator, and clim:define-presentation-action. This allows the pointer to be
used to input commands, including command arguments.
We say that a command is present in a command table when it has been added to
the command table by being associated with some form of interaction. We say that
a command is accessible in a command table when it is present in the command ta-
ble or is present in any of the command tables from which the command table in-
herits.




(command-table-p (object)
 (command-table object)


clim:command-table-name command-table
Returns the name of the command table command-table.
clim:command-table-inherit-from command-table
Returns a list of all of the command tables from which command-table
inherits.
clim:find-command-table name &key (:errorp t)
Returns the command table named by name.
clim:define-command-table name &key :inherit-from :menu :inherit-menu
Defines a new command table.
clim:make-command-table name &key :inherit-from :menu :inherit-menu (:errorp t)
Creates a command table named name that inherits from :inherit-from
and has a menu specified by :menu.
A command table can inherit from other command tables. This allows larger sets
