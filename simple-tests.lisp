(in-package :clim-user)

(define-application-frame test ()
  ()
  (:panes
   (display :application))
  (:layouts
   (default display)))

(define-test-command (com-quit :menu t) ()
  (frame-exit *application-frame*))

(defvar *test-frame* nil)

(defun test ()
  (flet ((run ()
	   (let ((frame (make-application-frame 'test)))
	     (setq *test-frame* frame) (run-frame-top-level frame))))
    (mp:process-run-function #'run)))

;;; evaluate the following to create and display the frame:
(test)

;;; after calling (test) and the frame has appeared evaluate...
(setq *test-pane* (get-frame-pane *test-frame* 'display))
